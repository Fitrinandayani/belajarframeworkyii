<div class="sub-content">
    <h2>Tambah Pegawai</h2>
    <?php echo CHtml::beginForm(array('pegawai/tambahpegawai')); ?>
    <div>
        <?php echo CHtml::activeLabel($model, 'nik'); ?>
        <?php echo CHtml::activeTextField($model, "nik", ""); ?>
    </div>

    <div>
        <?php echo CHtml::activeLabel($model, 'nama'); ?>
        <?php echo CHtml::activeTextField($model, "nama", ""); ?>
    </div>

    <div>
        <?php echo CHtml::activeLabel($model, 'no_telp'); ?>
        <?php echo CHtml::activeTextField($model, "no_telp", ""); ?>
    </div>

    <div>
        <?php echo CHtml::activeLabel($model, 'status'); ?>
        <?php echo CHtml::activeDropDownList($model, "status", array('Tetap' => 'Tetap', 'Kontrak' => 'Kontrak'), array('empty' => '- Pilih Status Pegawai -'))
        ?>
    </div>

    <div>
        <?php echo CHtml::activeLabel($model, 'alamat'); ?>
        <?php echo CHtml::activeTextField($model, "alamat", ""); ?>
    </div>

    <div>
        <?php echo CHtml::submitButton('Submit'); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>