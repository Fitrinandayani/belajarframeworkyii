<div class="sub-content">
    <h3>Pegawai</h3>
    <div class="text-right right">
        <?php echo CHtml::link('Tambah Pegawai', array('pegawai/tambahpegawai')); ?>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>NIK</th>
                <th>Nama</th>
                <th>No Telp</th>
                <th>Status</th>
                <th>Alamat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $model) : ?>
            <tr>
                <td><?php echo $model->nik; ?></td>
                <td><?php echo $model->nama; ?></td>
                <td><?php echo $model->no_telp; ?></td>
                <td><?php echo $model->status; ?></td>
                <td><?php echo $model->alamat; ?></td>
                <td>
                    <?php echo CHtml::link(CHtml::encode("Edit"), array('editpegawai', 'pegawai' => $model->nik)); ?> |
                    <?php echo CHtml::link(CHtml::encode("Delete"), array('hapuspegawai', 'pegawai' => $model->nik)); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>