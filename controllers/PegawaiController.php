<?php

namespace App\Http\Controllers;

class PegawaiController extends Controller {

public function actionPegawai() {
    $data = Pegawai::model()->findAll();
    $this->render('pegawai', array('data' => $data));
}

public function actionTambahPegawai() {
    $modelPegawai = new Pegawai;
    if (isset($_POST['Pegawai'])) {
        $modelPegawai->nama = $_POST['Pegawai']['nama'];    
        $modelPegawai->no_telp = $_POST['Pegawai']['no_telp'];
        $modelPegawai->status = $_POST['Pegawai']['status'];
        $modelPegawai->alamat = $_POST['Pegawai']['alamat'];
        if ($modelPegawai->save()) {
            $this->redirect(array('Pegawai'));
        }
    }

    $this->render('tambahpegawai', array('model' => $modelPegawai));
}

public function actionEditPegawai($nik) {
    $modelPegawai = Pegawai::model()->findByPk($nik);
    if (isset($_POST['Pegawai'])) {
        $modelPegawai->nama = $_POST['Pegawai']['nama'];
        $modelPegawai->no_telp = $_POST['Pegawai']['no_telp'];
        $modelPegawai->status = $_POST['Pegawai']['status'];
        $modelPegawai->alamat = $_POST['Pegawai']['alamat'];
        if ($modelPegawai->save()) {
            $this->redirect(array('pegawai'));
        }
    }

    $this->render('editpegawai', array('model' => $modelPegawai));
}

public function actionHapusPegawai($nik) {
    if (Pegawai::model()->deleteByPk($nik)) {
        $this->redirect(array('pegawai'));
    } else {
        throw new CHttpException(404, 'Data gagal dihapus');
    }
}
}