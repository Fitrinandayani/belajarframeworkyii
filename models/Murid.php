<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "murid".
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property string $nama_ortu
 * @property string $ttl
 */
class Murid extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'murid';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'alamat', 'nama_ortu', 'ttl'], 'required'],
            [['nama', 'alamat', 'nama_ortu', 'ttl'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'nama_ortu' => 'Nama Ortu',
            'ttl' => 'Ttl',
        ];
    }
}
