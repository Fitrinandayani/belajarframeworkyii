<?php

namespace app\models;

use Yii;

class Pegawai extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pegawai';
    }

    public function rules() {
        return array(
            array('nik, nama, no_telp, status, alamat', 'required', 'message' => '{attribute} tidak boleh kosong.'),
        );
    }

    public function attributeLabels() {
        return [
            'nik' => 'NIK',
            'nama' => 'Nama',
            'no_telp' => 'No Telp',
            'status' => 'Status',
            'alamat' => 'Alamat'
        ];
    }

}