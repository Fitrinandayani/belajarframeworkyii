<?php

use yii\db\Migration;

/**
 * Class m210522_052555_tabel_siswa
 */
class m210522_052555_tabel_siswa extends Migration
{
    public function up()
    {
        $this->createTable('siswa', [
            'id' => $this->primaryKey(),
            'nama'=> $this->string(30),
            'alamat'=> $this->string(50),
            'jenis_kelamin' => $this->string(30),
            'ttl' => $this->string(30),
        ]);
        $this->insert('siswa', [
            'nama'=> 'Ira Kesuma Dewi',
            'alamat'=> 'Medan',
            'jenis_kelamin' => "Perempuan",
            'ttl' => 'Kabanjahe, 22 Januari 1993',
        ]);
    }

    public function down()
    {
        $this->dropTable('siswa');
    }
}

